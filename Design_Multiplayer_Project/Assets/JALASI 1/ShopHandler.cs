﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ShopHandler : MonoBehaviour {
	public Text Txt_gold;
	//private int gold;
	public GameObject [] buy = new GameObject[4];
	public int [] harga;
	public GameObject [] canonUse;
	public GameObject [] bulletUse;
	public GameObject askingConfirm;
	private int confirmBuyed;
	private int confirmItem;
	// Use this for initialization

	void Start()
	{
		PlayerPrefs.SetInt ("Gold",33333);
		//PlayerPrefs.DeleteAll ();
		setAwalShop ();
	}

	void Update ()
	{
        

	}


	void setAwalShop()
	{
		//chek is buyed
		for (int i=0; i<4; i++)
		{
			if (PlayerPrefs.GetInt("buyed"+i.ToString())==1) {
                print("this is if:" + i);
				buy [i].SetActive (false);
                
			}
		}

		if (PlayerPrefs.GetInt ("UsingCanon") == 0)
			canonUse [0].SetActive (false);
		else if (PlayerPrefs.GetInt ("UsingCanon") == 1)
			canonUse [1].SetActive (false);
		else if (PlayerPrefs.GetInt ("UsingCanon") == 2)
			canonUse [2].SetActive (false);
		
		if (PlayerPrefs.GetInt ("UsingBullet") == 0)
			bulletUse [0].SetActive (false);
		else if (PlayerPrefs.GetInt ("UsingBullet") == 1)
			bulletUse [1].SetActive (false);
		else if (PlayerPrefs.GetInt ("UsingBullet") == 2)
			bulletUse [2].SetActive (false);


	}

	public void KonfirmasiPakai(int b)
	{
		if (b == 0) {
			askingConfirm.SetActive (false);
			return;
		}
		if (confirmBuyed == 1) {
			UseBullet (confirmItem+1);
		} 
		else 
		{
			UseCanon (confirmItem+1);
		}

		askingConfirm.SetActive (false);
	}


	public void BeliItem1 (int pil)
	{
		int gold = PlayerPrefs.GetInt ("Gold");
		if (gold >= harga [pil]) {
			buy [pil].SetActive (false);
			gold -= harga [pil];
			PlayerPrefs.SetInt ("Gold",gold);
			Txt_gold.text = "Gold: " + gold;

			if (pil == 0) { //dicek dilevel 2, jika buyedItem1 bernilai 1., maka asset default diganti Sprite
				PlayerPrefs.SetInt ("Peluru", 2);
				confirmBuyed = 1;
				confirmItem = 0;
			} 
			else if (pil == 1) {
				PlayerPrefs.SetInt ("Peluru", 3);
				confirmBuyed = 1;
				confirmItem = 1;
			} 
			else if (pil == 2) {
				PlayerPrefs.SetInt ("Canon", 2);
				confirmBuyed = 2;
				confirmItem = 0;
			} 
			else if (pil == 3) {
				PlayerPrefs.SetInt ("Canon3", 3);
				confirmBuyed = 2;
				confirmItem = 1;
			}

			PlayerPrefs.SetInt("buyed"+pil.ToString(),1);
			askingConfirm.SetActive (true);
		}

	}


	void UnUsed (string item)
	{
		if (item == "canon") {
			canonUse [0].SetActive (true);
			canonUse [1].SetActive (true);
			canonUse [2].SetActive (true);
			//enable all used canon.
			//set used canon.
		}
		else if (item == "bullet") {
			bulletUse [0].SetActive (true);
			bulletUse [1].SetActive (true);
			bulletUse [2].SetActive (true);
			//disable all used canon.
			//set used canon.
		}
	}

	public void UseCanon (int item)
	{
		UnUsed ("canon");
		canonUse [item].SetActive (false);
		PlayerPrefs.SetInt ("UsingCanon",item);
	}

	public void UseBullet (int item)
	{
		UnUsed ("bullet");
		bulletUse [item].SetActive (false);
		PlayerPrefs.SetInt ("UsingBullet",item);
	}


}
