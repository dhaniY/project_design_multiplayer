﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Pertanyaan : MonoBehaviour {
	public GameObject salahJawab;
	public GameObject SoalPanel;
	public Text goldText;


	public string[] soal;
	public string[] jawabanA;
	public string[] jawabanB;
	public int [] jawaban; //correct answer, fill 1 for A and 2 for B;

	public Text soalText;
	public Text jawabAText;
	public Text jawabBText;

	private int curSoal=0;
	private bool jwbLagi = true;
	private int gold;

	// Use this for initialization
	void Start () {
		gold = PlayerPrefs.GetInt ("Gold", 0);
		goldText.text = "Gold : " + gold.ToString ();
		randSoal ();
		setSoal ();
	}

	// Update is called once per frame
	void Update () {

	}

	public void jawab(int jwb)
	{
		if (!jwbLagi)
			return;

		if (jwb == jawaban [curSoal]) {
			//--//
			curSoal++;
			if (curSoal > 3-1) {
				SoalPanel.SetActive (false);
			}
			//--//-
			gold+=10;
			goldText.text = "Gold : " + gold.ToString ();
			//goldTextShop.text = "Gold : " + gold.ToString ();
			PlayerPrefs.SetInt ("Gold", gold);
			setSoal();
			print ("benar");
		} 
		else {
			//--//
			salahJawab.SetActive(true);
			Invoke ("jawabLagi", 1.5f);
			jwbLagi = false;
			//--//-
			print ("salah");
		}


	}


	void jawabLagi()
	{
		salahJawab.SetActive(false);
		jwbLagi = true;
	}

	void setSoal()
	{
		soalText.text = soal [curSoal];
		jawabAText.text = jawabanA[curSoal];
		jawabBText.text = jawabanB [curSoal];

	}

	void randSoal()
	{
		for (int i = 0; i < soal.Length-1; i++) {

            int SoalMin = PlayerPrefs.GetInt("Soal_Min");
            int SoalMax = PlayerPrefs.GetInt("Soal_Max");

			int rand = Random.Range (SoalMin, SoalMax);
			string tempsoal;
			string tempjawabanA;
			string tempjawabanB;
			int tempjawaban; 


			tempsoal = soal[i];
			tempjawabanA = jawabanA[i];
			tempjawabanB = jawabanB[i];
			tempjawaban = jawaban[i];
			//-------------------------//
			soal[i] = soal[rand];
			jawabanA [i] = jawabanA [rand];
			jawabanB [i] = jawabanB [rand];
			jawaban [i] = jawaban [rand];
			//-------------------------//
			soal[rand] = tempsoal;
			jawabanA [rand] = tempjawabanA;
			jawabanB [rand] = tempjawabanB;
			jawaban [rand] = tempjawaban;

		}
	}
}
