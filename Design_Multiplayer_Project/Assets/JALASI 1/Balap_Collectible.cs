﻿using UnityEngine;
using System.Collections;

public class Balap_Collectible : MonoBehaviour {

    public float CoinSpeed = 5;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        gameObject.transform.Translate(new Vector3(-CoinSpeed * Time.deltaTime, 0, 0));

        if (gameObject.transform.position.x <= -12)
        {
            Destroy(gameObject);
        }
	
	}

    
}
