﻿using UnityEngine;
using System.Collections;

public class SummonArea : MonoBehaviour
{
	public int min,max;
	public GameObject enemy;
	public GameObject[] Port;
	public int[] checkPort;
	void Start ()
	{
		int Random = UnityEngine.Random.Range(min,max);
		InvokeRepeating("summon",Random,Random);
	}

	public void Adds(int index){
		checkPort[index] = 1;
	}

	public void removes(int index){
		checkPort[index] = 0;
	}

	void summon(){
		GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
		for(int i = 0 ;i <= LevelCreator.levelDefenders ; i++){
		if(enemies.Length < 7){
		int Randoms = UnityEngine.Random.Range(-18,18);
		Vector3 xz = new Vector3(enemy.transform.position.x , enemy.transform.position.y + Randoms,enemy.transform.position.z);
		Instantiate(enemy,transform.position + xz,transform.rotation);
		}
	}
	}
	// Update is called once per frame
	void Update ()
	{
        if (Input.GetKeyDown(KeyCode.K))
        {
            Time.timeScale = 3;
        }
	}
}

