﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelCreator : MonoBehaviour
{

	// Use this for initializationd
	static public int levelDefenders;
	public int pondLevel;
	public int enemyStrength;
	public Text time;
	private float times;
	void Start ()
	{
		times = 60;
        if (levelDefenders == null || levelDefenders == 0 || levelDefenders !=0)
        {
			levelDefenders = 1;
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(times > 0){
			times -=  Time.deltaTime;
		}
		else{
			levelDefenders ++;
			times = 60;
		}

		if(levelDefenders >=3){
			Debug.Log("WIN");
			Time.timeScale = 0f;
            GameObject.Find("MANAGER").GetComponent<GameManager>().Evaluate();
		}

		int z = (int) times;
		time.text = "WAVE  " + levelDefenders + " : "  +   z.ToString() + "sec" ;
		gameObject.GetComponent<Text>().text = "Level " + levelDefenders;
	}
}

