﻿using UnityEngine;
using System.Collections;

public class Turtle : MonoBehaviour {

	public int HP = 10;
	public bool isMoved = false;
	public GameObject bullet;
	private Vector3 setDestination;
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
		//steer();
		if(GameManager.isGameover == false){
		Quaternion neededRotation = Quaternion.LookRotation(Vector3.forward, Vector3.zero - transform.position);
		transform.rotation = Quaternion.Slerp(transform.rotation,neededRotation, 1f * Time.deltaTime);
		//transform.LookAt(Vector3.zero,Vector3.up);
		if(Input.GetKeyDown(KeyCode.Space)){
			attack();
		}

	//	Vector3 move2 = new Vector3(Input.GetAxisRaw("Horizontal"),Input.GetAxisRaw("Vertical"),0f);
	//	transform.position += move2.normalized * Time.deltaTime;
	//	for (int i = 0; i < Input.touchCount; ++i) {
	//		Debug.Log("WAEEEE");
	//		Touch touch = Input.GetTouch(0);
	//		if (touch.phase == TouchPhase.Began) { 
		if(Input.GetMouseButtonDown(0) && UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject() == false){
			Vector3 temp =  Camera.main.ScreenToWorldPoint(Input.mousePosition);
				setDestination = new Vector3(temp.x,temp.y,0f);

				isMoved = true;
		}
	//	}
		dash();
		}
	}


	void OnTriggerEnter2D(Collider2D col){
		if(col.gameObject.tag == "bulletEnemy"){
			HP -= 1;
			Destroy(col.gameObject);
			if(HP<=0){
				GameObject.FindObjectOfType<Shake>().shakes();
				Destroy(gameObject);
			}
		}
	}

	public	void attack(){
		GameObject bult = Instantiate(bullet);
		bult.transform.position = gameObject.transform.position;
		bult.transform.localRotation = gameObject.transform.localRotation;

	}

	void OnCollisionEnter2D(Collision2D col){
		Enemy enemy = col.gameObject.GetComponent<Enemy>();
		if(enemy !=null && enemy.isStrong){
			HP-= 1;
		}
		else if(enemy!= null && enemy.isStrong == false){
			Destroy(enemy.gameObject);
		}
	}

	void steer(){
		gameObject.transform.Rotate(0f,0f,Input.GetAxis("Horizontal"));
	}

	void dash(){


		if(isMoved){
			transform.position = Vector3.Lerp(transform.position,setDestination,  Time.deltaTime);
		if(Vector2.Distance(transform.position,setDestination) < 0.000001f){
		isMoved = false;
		}
		}
	}





}
