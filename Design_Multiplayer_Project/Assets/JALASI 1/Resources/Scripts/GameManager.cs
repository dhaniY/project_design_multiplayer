﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour
{


    public int Hp, Fish, Poin, FishMax, cost, LevelGameDefense;
	public int currentUpgrade = 1;
	static public bool isGameover,isPause=false;
	public Button cos;
	public Image HpI,FishI;
	public Text HpT,FishT,PoinT,costs;
	public Image Restart;
	private Turtle test2;
    public GameObject PanelConfirm,PanelPause;
	public Quiz MyQuiz;
	private int stage;
	private float time;
	// Use this for initialization
	void Start ()
	{
        PlayerPrefs.SetInt("IsAfterFishing", 1);
        PlayerPrefs.SetInt("NowLevelWhat", 5);
		MyQuiz =GameObject.FindObjectOfType<Quiz>();
        Time.timeScale = 0;
		isGameover = false;
		Poin = 0;
//		FishMax = (LevelCreator.levelDefenders * 5)

	}

	public void answerTrue(){
			GameObject.Find("Player"+ currentUpgrade).GetComponent<CannonStrike>().enabled = true;
		  	GameObject.Find("Player" + currentUpgrade).transform.GetChild(0).gameObject.SetActive(true);
			currentUpgrade ++;
	}

	public void upgrade(){
		if(Poin >= cost){
			MyQuiz.showSoalDefender();
		}
	}


	public void restart(){
		Application.LoadLevel(Application.loadedLevel); // aku lali ganti iki nggo opo scenemanager lek ga salah cuma lali.
	}
	// Update is called once per frame
	void Update ()
	{
		if(GameObject.Find("Player") != null){
		Hp = GameObject.Find("Player").GetComponent<Turtle>().HP;
		}
		HpI.fillAmount = Hp/10f;
		FishI.fillAmount = Fish/10f;
		if(Hp <= 0 || Fish <= 0){
			isGameover = true;
			Restart.gameObject.SetActive(true);
		}
		PoinT.text = Poin.ToString();
		cost = (10 + (currentUpgrade * 50));


		if(currentUpgrade < 4){
		costs.text = "Upgrade " + cost.ToString();
		}
		else{
		costs.text = "Upgrade Max ";
		}



		if(Poin >= cost && currentUpgrade < 4){
			cos.interactable = true;
		}
		else{
			cos.interactable = false;
		}
	}


    public void Evaluate()
    {
        int savePoint = Poin;
        if (savePoint <= 30)
            PlayerPrefs.SetInt("pointBintang", 1);
        else if (savePoint <= 60)
            PlayerPrefs.SetInt("pointBintang", 2);
        else
            PlayerPrefs.SetInt("pointBintang", 3);


        PlayerPrefs.SetInt("LevelGameDefense", LevelGameDefense);
        if (LevelGameDefense == 1)
        {
            PlayerPrefs.SetInt("LastPoint_Defense_1", savePoint);
        }
        if (LevelGameDefense == 2)
        {
            PlayerPrefs.SetInt("LastPoint_Defense_2", savePoint);
        }

        Application.LoadLevel("Achievement");

    }


    public void ConfirmPlay()
    {
        Time.timeScale = 1;
        PanelConfirm.SetActive(false);
    }

    public void PauseEffect()
    {
        if (!isPause)
        {
            PanelPause.SetActive(true);
            Time.timeScale = 0;
        }
        isPause = !isPause;
    }

    public void ResumeEffect()
    {
        if (isPause)
        {
            PanelPause.SetActive(false);
            Time.timeScale = 1;
        }
        isPause = !isPause;
    }

    public void MenuEffect()
    {
        Application.LoadLevel("MainMenu");
    }
}

