﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Enemy : MonoBehaviour
{

	public float hp;
	public bool isStrong = false;
	public GameObject barCatch,bullets;


	private float scaleX;
	private float cooldown = 5f;
	private float cooldownCatch = 5f;
	private float cooldownAttack = 3f;
	private GameObject player;
	private Vector3 target;
	public List<int> freePort;
	private SummonArea summon;
	private int targets;
	// Use this for initialization
	void Start ()
	{
	 summon = GameObject.FindObjectOfType<SummonArea>();

		freePort.Clear();

		for(int i = 0; i < summon.checkPort.Length;i++){
			if(summon.checkPort[i] == 0){
				freePort.Add(i);
			}
		}

		int rand = UnityEngine.Random.Range(0,freePort.Count);
		targets = freePort[rand];
		summon.Adds(targets);

		gameObject.name = "BoatToFishArea" +  targets;

		hp = hp + LevelCreator.levelDefenders * 3;
		scaleX = barCatch.transform.localScale.x;
		player = GameObject.FindGameObjectWithTag("Pond");
		GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
		if(enemies != null){
			if(targets + 1 > 0 && targets + 1 < 4){
				gameObject.transform.position = GameObject.Find("SummonArea1").transform.position;
			}
			else if(targets + 1 >= 4 && targets + 1 < 6){
				gameObject.transform.position = GameObject.Find("SummonArea2").transform.position;
			}
			else if(targets+ 1 >= 6 && targets + 1 < 8){
				gameObject.transform.position = GameObject.Find("SummonArea3").transform.position;
			}
			else if(targets + 1 >= 8){
				gameObject.transform.position = GameObject.Find("SummonArea4").transform.position;
			}







			target =  GameObject.Find("FishArea" + (targets+1)).transform.position;//new Vector3(UnityEngine.Random.Range(-8,8), UnityEngine.Random.Range(-8,8),0f);
		}
		else{
			target =  GameObject.Find("FishArea1").transform.position;
		}
	}



	void findSummonArea(){


	}

	
	// Update is called once per frame
	void Update ()
	{
		if(GameManager.isGameover == false){
		if(transform.position == target){
		strongCooldown();
		catchFish();
//Attack();
		}
		if(GameObject.Find("FishArea") != null){
		Quaternion neededRotation = Quaternion.LookRotation(Vector3.forward, GameObject.Find("FishArea").transform.position - transform.position);
		transform.rotation = Quaternion.Slerp(transform.rotation,neededRotation, 1f * Time.deltaTime);
		}
		MoveTo();
		barCatch.transform.localScale = new Vector3(cooldownCatch/5f,1f,0f) * scaleX;
	}
	}


	void OnTriggerStay2D(Collider2D col){
		if(col.gameObject.tag == "bullet"){
			hp -=  0.8f;
			//Destroy(col.gameObject);
			if(hp<=0){
				GameObject.FindObjectOfType<Shake>().shakes();
				GameObject.FindObjectOfType<GameManager>().Poin += 10;
				summon.removes(targets);
				Destroy(gameObject);
			}
		}
	}





	void Attack(){
		cooldownAttack -= Time.deltaTime;
		if(cooldownAttack <= 0 && isStrong){
			if(Vector3.Distance(transform.position,player.transform.position) < 6f){

				GameObject enemyBullet = Instantiate(bullets);
				enemyBullet.transform.position = transform.position;
				enemyBullet.transform.rotation = transform.rotation;
			//	Quaternion neededRotation = Quaternion.LookRotation(Vector3.forward, GameObject.Find("Player").transform.position - enemyBullet.transform.position);
			//	enemyBullet.transform.rotation = Quaternion.Slerp(enemyBullet.transform.rotation,neededRotation, 1f * Time.deltaTime);

			cooldownAttack = 3;
			}}
	}

	void catchFish(){
		barCatch.GetComponent<SpriteRenderer>().enabled = true;
		cooldownCatch -= Time.deltaTime;
		if(cooldownCatch <= 0){
			GameObject.FindObjectOfType<GameManager>().Fish -=1;
			cooldownCatch = 5;
		}
	}



	void MoveTo(){
		transform.position = Vector3.MoveTowards(transform.position,target,Time.deltaTime * 2.2f);
 	}

	void strongCooldown(){


		cooldown -= Time.deltaTime;
		if(cooldown <= 0){
			if(isStrong){
				gameObject.GetComponent<SpriteRenderer>().color = Color.white;
				isStrong = false;
				cooldown = 5f;

			}
			else{
				gameObject.GetComponent<SpriteRenderer>().color = Color.red;
				isStrong = true;
				cooldown = 5f;
			}
		}
	}




}

