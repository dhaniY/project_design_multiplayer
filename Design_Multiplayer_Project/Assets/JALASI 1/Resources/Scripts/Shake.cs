﻿using UnityEngine;
using System.Collections;

public class Shake : MonoBehaviour
{


	public Vector3 originalCameraPosition = new Vector3(0f,0f,-10f);
	Quaternion originalRotation;
	public float sha ;
	public bool isShake = false;
	float shakeAmt = 0.32f;


	public void shakes(){
		isShake = true;
		Invoke("StopShaking",0.4f);
	}
	void Update(){
		if(isShake){
		CameraShake();
		}
	}

	void CameraShake()
	{
		if(shakeAmt>0) 
		{
			float z = Random.insideUnitCircle.x * sha; 
			float x = Random.insideUnitSphere.y * sha;
			gameObject.transform.localPosition = new Vector3(transform.position.x + z * shakeAmt, transform.position.y + x  *shakeAmt,-10f) ;
			//	shake -= Time.deltaTime * decreaseFactor;
		}
	}

	void StopShaking()
	{
		CancelInvoke("CameraShake");
		isShake = false;
		gameObject.transform.position = originalCameraPosition;
		gameObject.transform.rotation = originalRotation;
	}

}

