﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

    #region BACKGROUND_MUSIC

    public AudioClip[] backgroundMusicClipsArray;
    public AudioSource backgroundMusicSource;

    #endregion

    #region SFX_SOUNDS

    public AudioClip[] SFXSounds;
    public AudioSource SFXAudioSource;

    #endregion

    #region PUBLIC_METHODS

    public void PlayMusic()
    {
        if (PlayerPrefs.GetInt("BGM_INGAME") == 1)
        {
            backgroundMusicSource.clip = backgroundMusicClipsArray[0];
            backgroundMusicSource.Play();
        }
    }

    public void PlaySFXSounds(int NumberSound)
    {
        if (PlayerPrefs.GetInt("SFX_INGAME") == 1)
        {
            SFXAudioSource.PlayOneShot(SFXSounds[NumberSound]);
        }
    }

    #endregion
}