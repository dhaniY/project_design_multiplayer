﻿using UnityEngine;
using System.Collections;

public class Balap_BackgroundControl : MonoBehaviour {

	// Use this for initialization
    public GameObject[] BGObject;
    public float Speed;
	public void StartGame () {

        StartCoroutine(SpeedIncrease());
	
	}
	
	// Update is called once per frame
	void Update () {

        if (GameObject.Find("GameManager").GetComponent<Balap_GameManager>().GameStart == true)
        {
            
            BGObject[0].transform.Translate(new Vector3(-1 * Time.deltaTime * Speed, 0, 0));
            BGObject[1].transform.Translate(new Vector3(-1 * Time.deltaTime * Speed, 0, 0));
            BGObject[2].transform.Translate(new Vector3(-1 * Time.deltaTime * Speed, 0, 0));
            BGObject[3].transform.Translate(new Vector3(-1 * Time.deltaTime * Speed, 0, 0));
        }

        if (BGObject[0].transform.position.x <= -30)
        {
            BGObject[0].transform.position = new Vector3(BGObject[3].transform.position.x + 15.8f,BGObject[0].transform.position.y,BGObject[0].transform.position.z);
        }

        if (BGObject[1].transform.position.x <= -30)
        {
            BGObject[1].transform.position = new Vector3(BGObject[0].transform.position.x + 15.8f, BGObject[1].transform.position.y, BGObject[1].transform.position.z);
        }

        if (BGObject[2].transform.position.x <= -30)
        {
            BGObject[2].transform.position = new Vector3(BGObject[1].transform.position.x + 15.8f, BGObject[2].transform.position.y, BGObject[2].transform.position.z);
        }

        if (BGObject[3].transform.position.x <= -30)
        {
            BGObject[3].transform.position = new Vector3(BGObject[2].transform.position.x + 15.8f, BGObject[3].transform.position.y, BGObject[3].transform.position.z);
        }

        
	
	}

    IEnumerator SpeedIncrease()
    {
        while (Speed <= 10)
        {
            yield return new WaitForSeconds(0.01f);
            Speed += 1;
        }
    }
}
