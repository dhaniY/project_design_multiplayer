﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Balap_GameManager : MonoBehaviour {

	// Use this for initialization
    bool isPause = false;
    public bool GameStart=false;
    public bool isBoostEffect = false;

    public float SpeedX;
    public float BoostTime = 2;
    public float Game_TimeScale = 1;
    public float DistancePlayer = 0;
    public float MyTime=0;

    public int PoinPlayer=0;
    public int AdderPoin=10;

	public Text soalText;
	public Text jawabAText;
	public Text jawabBText;

	public string[] soal;
	public string[] jawabanA;
	public string[] jawabanB;
	public int curSoal;
    public GameObject[] KapalMusuh,CoinsObject;
    public GameObject StartObject, FinishObject;
    public GameObject PanelConfirm,PanelPause;

    public Button BoostButton;
    Quiz MyQuiz;
    
    public Text Text_Distance, Text_Point, Text_Health;

    public GameObject number_anim;

	void Start () {
        PlayerPrefs.SetInt("IsAfterFishing", 1);
        Time.timeScale = 0;
        PlayerPrefs.SetInt("NowLevelWhat", 4);
        MyQuiz =GameObject.Find("GameManager").GetComponent<Quiz>();
        GamingHubClient.Instance.InstantiateAllPlayer();
        MyTime = (int)Time.realtimeSinceStartup;
        InvokeRepeating("SpawningCoins", 1,3f);
	}
	
	// Update is called once per frame
	void Update () {

        if (CheckPlayer())
        {
            ConfirmPlay();
        }
        
        DistancePlayer += 1;
        Text_Distance.text = "Jarak : " + (int) Time.realtimeSinceStartup + " m";
        Text_Point.text = "Poin : " + PoinPlayer;

#region Key
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.LoadLevel("LevelSelect");
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            BoostEffectButton();
        }
#endregion

        if (isBoostEffect)
        {
            Time.timeScale = Game_TimeScale+1;
            StartCoroutine(BoostEffect());
        }else
            Time.timeScale = Game_TimeScale;

        if (StartObject != null) { 

        if (StartObject.transform.position.x <= GameObject.Find("batasHilang").transform.position.x)
        {
            Destroy(StartObject);
        }
        }

        #region GameObject Race
        if (GameStart) { 
                if (KapalMusuh[0].transform.position.x >= -10)
                {
                    KapalMusuh[0].transform.Translate(new Vector3(-1 * SpeedX*Time.deltaTime, 0, 0));
                }
                else
                {
                    KapalMusuh[0].transform.position = (new Vector3(Random.Range(10, 40), KapalMusuh[0].transform.position.y, KapalMusuh[0].transform.position.z));
                }

                if (KapalMusuh[1].transform.position.x >= -10)
                {
                    KapalMusuh[1].transform.Translate(new Vector3(-1 * SpeedX * Time.deltaTime, 0, 0));
                }
                else
                {
                    KapalMusuh[1].transform.position = (new Vector3(Random.Range(10, 20), KapalMusuh[1].transform.position.y, KapalMusuh[1].transform.position.z));
                }

                if (KapalMusuh[2].transform.position.x >= -10)
                {
                    KapalMusuh[2].transform.Translate(new Vector3(-1 * SpeedX * Time.deltaTime, 0, 0));
                }
                else
                {
                    KapalMusuh[2].transform.position = (new Vector3(Random.Range(10, 70), KapalMusuh[2].transform.position.y, KapalMusuh[2].transform.position.z));
                }

                if (KapalMusuh[3].transform.position.x >= -10)
                {
                    KapalMusuh[3].transform.Translate(new Vector3(-1 * SpeedX * Time.deltaTime, 0, 0));
                }
                else
                {
                    KapalMusuh[3].transform.position = (new Vector3(Random.Range(10, 90), KapalMusuh[3].transform.position.y, KapalMusuh[3].transform.position.z));
                }

                

            }
        #endregion

       
	
	}
    bool CheckPlayer()
    {
        bool tempStatus = false;
        foreach (var player in GamingHubClient.Instance.players)
        {
            if (player.Value.isReady)
            {
                tempStatus = true;
            }
            else
            {
                tempStatus = false;
            }
        }
        return tempStatus;
    }
    IEnumerator WaitForStart()
    {
        number_anim.GetComponent<Animator>().SetBool("IsReady", true);
        float timeOfAnimation = number_anim.GetComponent<Animator>().runtimeAnimatorController.animationClips[0].length + 0.5f;
        yield return new WaitForSeconds(timeOfAnimation);
        number_anim.SetActive(false);
        GameStart = true;
        GameObject.Find("BackgroundLand").GetComponent<Balap_BackgroundControl>().StartGame();
        
    }

    public void BoostEffectButton()
    {
        isBoostEffect = true;
        BoostButton.interactable = false;
    }
    IEnumerator BoostEffect()
    {
        
        yield return new WaitForSeconds(BoostTime);
        isBoostEffect = false;
        BoostButton.interactable = true;
    }

    public void AddPoint()
    {
        PoinPlayer += AdderPoin;
    }
	public void AddPoint2(int koinIstimewa)
	{
		PoinPlayer += koinIstimewa;
	}
	public void setSoal()
	{
		soalText.text = soal [curSoal];
		jawabAText.text = jawabanA[curSoal];
		jawabBText.text = jawabanB [curSoal];

	}
    public void SpawningCoins()
    {
//Spawn not only coin but obstacle
		int index = 0;
		//30% chance obstacle 70% chance coin
		int randoms = UnityEngine.Random.Range(0,10);
		if(randoms > 5 && randoms < 8){
			index = 1;
		}
		//else if (randoms >= 8){
		//	index = 2;
		//}


        Transform spawnerPos = GameObject.Find("CoinSpawner").GetComponent<Transform>();
		Instantiate(CoinsObject[index],new Vector3(spawnerPos.position.x,spawnerPos.position.y+Random.Range(-2,4),spawnerPos.position.z),Quaternion.identity);
    }

    public void ConfirmPlay()
    {
        Time.timeScale = 1;
        StartCoroutine(WaitForStart());
        PanelConfirm.SetActive(false);
    }

    public void PauseEffect()
    {
        if (!isPause)
        {
            PanelPause.SetActive(true);
            MyQuiz.soalShown = true;
        }
        isPause = !isPause;
    }

    public void ResumeEffect()
    {
        if (isPause)
        {
            PanelPause.SetActive(false);
            MyQuiz.soalShown = false;
        }
        isPause = !isPause;
    }

    public void MenuEffect()
    {
        Application.LoadLevel("MainMenus");
    }

}
