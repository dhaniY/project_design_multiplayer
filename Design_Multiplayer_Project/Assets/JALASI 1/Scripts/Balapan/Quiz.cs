﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class Quiz : MonoBehaviour
{
	public string[] soal,jwb1,jwb2,/*jwb3*/answer;
	public GameObject bases;
	public int curQuiz;
    public bool soalShown=false;
	public Text textSoal,textJwb1,textJwb2/*,textJwb3*/;
	// Use this for initialization
	void Start ()
	{
		//showSoal();
	}


	public void showSoal(){
		if(bases.activeSelf == false && curQuiz<7){
		bases.SetActive(true);
		getSoal();
		getChoice();
        soalShown = true;
        
		}
	}

	public void showSoalDefender(){
		Debug.Log("WEW");
		curQuiz = UnityEngine.Random.Range(0,soal.Length);
		if(bases.activeSelf == false){
			bases.SetActive(true);
			getSoal();
			getChoice();
			soalShown = true;

		}
	}
	public void  isAnswer(){
		if(EventSystem.current.currentSelectedGameObject.GetComponentInChildren<Text>().text == answer[curQuiz]){
			GameObject.FindObjectOfType<Balap_GameManager>().AddPoint2(50);
			curQuiz += 1;
			bases.SetActive(false);
			Time.timeScale = 1f;
            soalShown = false;
		}
		else{
			GameObject.FindObjectOfType<Shake>().shakes();
			bases.SetActive(false);
			curQuiz += 1;
			Time.timeScale = 1f;
            soalShown = false;
		}
	}


	public void  isAnswerDefender(){
		if(EventSystem.current.currentSelectedGameObject.GetComponentInChildren<Text>().text == answer[curQuiz]){
			GameObject.FindObjectOfType<GameManager>().answerTrue();
			//curQuiz += 1;
			bases.SetActive(false);
			Time.timeScale = 1f;
			soalShown = false;
		}
		else{
			GameObject.FindObjectOfType<Shake>().shakes();
			bases.SetActive(false);
		//	curQuiz += 1;
			Time.timeScale = 1f;
			soalShown = false;
		}
	}




	void getSoal(){
		textSoal.text = soal[curQuiz];
	}

	void getChoice(){
		textJwb1.text = jwb1[curQuiz];
		textJwb2.text = jwb2[curQuiz];
		//textJwb3.text = jwb3[curQuiz];

	}

	// Update is called once per frame
	void Update ()
	{

        if (soalShown)
        {      
            Time.timeScale = 0;
        }
	}

}

