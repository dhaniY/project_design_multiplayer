﻿using UnityEngine;
using System.Collections;

public class CameraShaker : MonoBehaviour {

	// Use this for initialization
    public float shakeTimer;
    public float shakeAmount;

    Vector3 posAwal;
    //bool Goyang = false;

	void Start () {

        posAwal = this.transform.position;
	
	}
	
	// Update is called once per frame
	void Update () {

        if (shakeTimer >= 0)
        {
            Vector3 ShakePos = Random.insideUnitCircle * shakeAmount;

            transform.position = new Vector3(transform.position.x + ShakePos.x, transform.position.y+ ShakePos.y, transform.position.z + ShakePos.z);

            shakeTimer -= Time.deltaTime;

        }

        if (shakeTimer <= 0)
        {
            this.transform.position = posAwal;
        }
	
	}

    public void ShakeCamera(float ShakePwr, float ShakeDur)
    {
        shakeAmount = ShakePwr;
        shakeTimer = ShakeDur;
    }

    public void startShaking()
    {
        ShakeCamera(0.05f, 0.5f);
    }
}
