﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingManager : MonoBehaviour {

	// Use this for initialization
	
	private float PB_xSize;  
    [SerializeField]
	private int count=0;
	//private RectTransform myRect;
    public Image fillBarr;
    public GameObject ButtontoLoadLevel;

    public GameObject Button_Next, Button_Back;
    //public GameObject ProgressBar;

    public bool CanNext;

    //public int LevelLoad;
    string LevelToLoad;
    [SerializeField]
    private int LevelLoadedText = 0;

    public Text TextContent;
    private string[] ContentLoading = new string[]{
        //NATUNA 0
        "Natuna disamping sebagai nama pulau, juga sebagai nama salah satu kabupaten yang tergabung dalam Provinsi Kepulauan Riau. Pulau yang tergabung dalam gugusan Pulau Tujuh ini berada di lintasan jalur pelayaran internasional dari dan atau menuju Hongkong, Taiwan, Jepang, dan Korea. ",
        "Di sebelah utara, Natuna berbatasan dengan Vietnam dan Kamboja, di selatan berbatasan dengan Sumatera Selatan dan Jambi, di bagian barat dengan Singapura, Malaysia, Riau dan di bagian timur dengan Malaysia Timur dan Kalimantan Barat. ",
        "Kabupaten ini terkenal dengan penghasil minyak dan gas. Berdasarkan kondisi fisiknya, Kabupaten Natuna merupakan tanah berbukit dan bergunung batu. Dataran rendah dan landai banyak ditemukan di pinggir pantai. ",
        "Iklim di Kabupaten Natuna adalah tropis basah dengan suhu rata-rata 26 °C dan sangat dipengaruhi oleh perubahan arah angin. Kelembaban udaranya berkisar antara 60% dan 85%. Sedangkan, curah hujannya rata-rata 2.530 mm dengan jumlah hari hujan 110 pertahun. ",
        "Cuacanya sering tidak menentu. Hujan disertai angin kencang, badai yang bergemuruh, dan gelombang yang mencapai ketinggian lebih dari tiga meter seringkali terjadi secara tiba-tiba. ",
        //SEBATIK 5
        "Sebatik adalah sebuah pulau di sebelah timur laut Pulau Kalimantan. Pulau ini merupakan salah satu pulau terluar yang menjadi prioritas utama pembangunan karena berbatasan langsung dengan negara tetangga.  ",
        "Belahan utara seluas 187,23 km persegi merupakan wilayah Negara Bagian Sabah, Malaysia, sedangkan belahan selatan dengan luas 246,61 km persegi masuk ke wilayah Indonesia, tepatnya Kabupaten Nunukan, Provinsi Kalimantan Utara. ",
        "Diwilayah Pulau Sebatik terumbu karang berada di daerah sekitar pesisir Pantai Batu Lamampu. Ekosistem hutan bakau di Sebatik menyebar tidak merata di seluruh pantai dan pesisir. ",
        "Beberapa jenis pohon bakau yang umum dijumpai di Sebatik adalah bakau (Rhizophora spp), api-api (Avicennia spp), tanjung (Bruguiera spp), tengar (Ceriops spp), dan buta-buta (Exoecaria spp).",
        "Pulau Sebatik terbagi menjadi dua bagian.",
        //MOROTAI 10
        "Pulau Morotai merupakan salah satu pulau terbesar di Maluku Utara yang memiliki potensi sumber daya alam yang cukup melimpah, baik di sektor pertanian, kehutanan, perikanan dan kelautan, pertambangan maupun potensi pariwisata sejarah terutama tempat-tempat sejarah peninggalan Perang Dunia Kedua. ",
        "Pulau Morotai sebagian besar berupa hutan dan memproduksi kayu serta damar dan sangat strategis sebagai jalur perdagangan di timur Indonesia. ",
        "Dari aspek geografis pulau Morotai memiliki posisi strategis karena berada di bibir jalur perdagangan Asia Pasifik. Batas-batas administrasi yang dimiliki oleh kabupaten ini adalah sebagai berikut :",
        "\n\nSebelah Utara : Samudera Pasifik\nSebelah Barat : Laut Sulawesi\nSebelah Timur : Laut Halmahera \nSebelah Selatan : Selat Morotai",
        "Kabupaten Pulau Morotai mempunyai luas wilayah 4.301,53 Km2 dengan panjang garis pantai 311.217 Km. Jumlah pulau-pulau kecil yang terdapat di Kabupaten Pulau Morotai berjumlah 33 pulau dengan rincian pulau yang berpenghuni berjumlah 7 pulau dan yang tidak berpenghuni berjumlah 26 pulau. ",
        //TANIMBAR 15
        "Kepulauan Tanimbar adalah bagian dari gugus pulau di Provinsi Maluku yang terletak di bagian selatan.  Panjang garis pantai kepulauan Tanimbar adalah 1623,2695 km. Kepulauan Tanimbar berbatasan langsung dengan : ",
        "Utara : Laut Banda \nSelatan : Laut Timor dan Arafura \nBarat : gugus Pulau Babar Sermata, Kab Maluku Barat Daya \nTimur : Laut Arafura",
        "Gugus pulau Tanimbar terdiri dari 174 pulau baik yang berpenghuni maupun tidak berpenghuni . Dari total pulau tersebut, hanya 1 pulau yang tergolong pulau besar sekaligus sebagai daratan utama (mainland) yakni Pulau Yamdena, ",
        "sedangkan sisanya tergolong sebagai Pulau Kecil (≤ 2000 km2) sebagaimana kriteria yang terdapat dalam UU No. 27 Tahun 2007 tentang Pengelolaan Wilayah Pesisir dan Pulau-Pulau Kecil. Pulau Yamdena memiliki luas 3.333 km² yang membujur dari utara ke selatan. Terdapat dua kota besar di Pulau Yamdena yaitu Saumlaki dan Larat . ",
        "Mata pencaharian masyarakat Tanimbar adalah berladang dan menangkap ikan. Sebagai pulau bertanah kapur yang terbentuk di atas karang, sebagian besar pulau-pulau di Tanimbar tak mudah ditanami palawija seperti padi dan jagung, tetapi subur bagi ubi, pisang, dan sukun. Protein hewani mereka peroleh dari hasil tangkapan ikan dan binatang laut lainnya.",
        //BAWEAN 20
        "Bawean adalah sebuah pulau yang terletak di Laut Jawa, sekitar 80 Mil atau 120 kilometer sebelah utara Gresik. Pulau ini masuk dalam wilayah kabupaten Gresik. Kata Bawean berasal dari Bahasa Sansekerta, yang berarti ada sinar matahari. Bawean sering disebut juga Pulau Putri karena banyak laki-laki muda yang merantau ke pulau Jawa atau ke luar negeri seperti Malaysia dan Singapura.  ",
        "Di Bawean terdapat spesies rusa yang hanya ditemukan (endemik) di Bawean, yaitu Axis kuhli. Selain itu di Pulau Bawean juga banyak ditanam manggis, salak, buah merah, dan durian untuk konsumsi lokal. ", 
        "Meski memiliki potensi wisata yang sangat melimpah, Pulau Bawean masih cenderung belum terlalu terekspos. ",
        "Beberapa obyek wisata yang menarik di Pulau Bawaean antara lain adalah Tanjung Gaang, Pantai Ria, Pantai Mayangkara, Pantai Kuburan Panjang, Danau Kastoba, Air Terjun Laccar, dan Penangkaran Rusa Bawean. ",
        "Puluhan spesies ikan laut juga terdapat di pantai pulau ini. Salah satu yang khas dari Bawean adalah batu onyx, yaitu sejenis batu marmer. Batu ini umumnya dijadikan hiasan dan juga lantai. ",
        //MENTAWAI 25
        "Mentawai merupakan nama sebuah kepulauan sekaligus nama sebuah kabupaten yang terletak memanjang dibagian paling barat Pulau Sumatera dan dikelilingi oleh Samudera Hindia. ",
        "Kabupaten Kepulauan Mentawai terdiri atas 4 kelompok pulau utama yang berpenghuni yaitu Pulau Siberut, Pulau Sipora, Pulau Pagai Utara, dan Pulau Pagai Selatan. Selain itu masih ada beberapa pulau kecil lainnya yang berpenghuni namun sebagian besar pulau yang lain hanya ditanami dengan pohon kelapa. ",
        "Di Pulau Siberut ada taman nasional yang dijadikan salah satu cagar biosfer oleh UNESCO. Taman nasional ini merupakan rumah bagi 18 satwa langka yang tidak bisa dilihat di tempat lain. Satwa langka ini termasuk tupai terbang dan monyet ekor babi. Hasil laut merupakan salah satu potensi yang terus dikembangkan di Kabupaten Kepulauan Mentawai ini terutama Ikan Kerapu yang laku untuk di ekspor.",
        "Olahraga Selancar merupakan salah satu atraksi wisata bahari andalan Kabupaten kepulauan Mentawai. Hal ini disebabkan letak geografis Kepulauan Mentawai yang berhadapan langsung dengan Samudera Hindia sehingga karateritik gelombangnya yang tinggi dan besar banyak diminati oleh peselancar dunia. ",
        "Dari 71 titik selancar yang ada di Kepulauan Mentawai, 49 titik diantaranya masuk katagori executive berskala internasional dan 7  titik (spot) diantaranya merupakan yang terbaik dari 10 titik spot yang ada di dunia. Sehingga tidaklah berlebihan jika setiap tahunnya Kepulauan Mentawan di jadikan event super series atau Kejuaraan Selancar Dunia yang biasanya diselenggarakan setiap bulan Agustus.",
        //KOMODO 30
        "Pulau Komodo adalah sebuah pulau yang terletak di Kepulauan Nusa Tenggara. Pulau Komodo dikenal sebagai habitat asli hewan komodo. Pulau ini juga merupakan kawasan Taman Nasional Komodo yang dikelola oleh Pemerintah Pusat. Pulau Komodo berada di sebelah timur Pulau Sumbawa, yang dipisahkan oleh Selat Sape. ",
        "Secara administratif, pulau ini termasuk wilayah Kecamatan Komodo, Kabupaten Manggarai Barat, Provinsi Nusa Tenggara Timur,Indonesia. Pulau Komodo merupakan ujung paling barat Provinsi Nusa Tenggara Timur, berbatasan dengan Provinsi Nusa Tenggara Barat.",
        "UNESCO mengakui Pulau Komodo sebagai Situs Warisan Dunia pada 1986. Bersama dua pulau besar lainnya, yakni Pulau Rinca dan Padar, Pulau Komodo dan beberapa pulau kecil di sekitarnya terus dipelihara sebagai habitat asli Komodo.",
        "Sebenarnya daya tarik Taman Nasional Komodo tidak semata-mata oleh kehadiran Komodo belaka. Panorama savana dan pemandangan bawah laut merupakan daya tarik pendukung yang potensial. Wisata bahari misalnya, memancing, snorkeling, diving, kano, bersampan.",
        "Sedangkan di daratan, potensi wisata alam yang bisa dilakukan adalah pengamatan satwa, hiking, dan camping. ",
        //DERAWAN 35
        "Kepulauan Derawan adalah sebuah kepulauan yang berada di Kabupaten Berau, Kalimantan Timur. Di kepulauan ini terdapat sejumlah objek wisata bahari yang menawan, salah satunya adalah taman bawah laut yang diminati wisatawan mancanegara terutama para penyelam kelas dunia.",
        "Setidaknya ada empat pulau yang terkenal di Kepulauan Derawan tersebut, yakni Pulau Derawan, Maratua, Sangalaki, dan Kakaban. Di Kepulauan Derawan terdapat beberapa ekosistem pesisir dan pulau kecil yang sangat penting yaitu terumbu karang, padang lamun, dan hutan bakau (hutan mangrove). ",
        "Selain itu banyak spesies yang dilindungi berada di Kepulauan Derawan seperti penyu hijau, penyu sisik, paus, lumba-lumba, kima, ketam kelapa, duyung, ikan barakuda dan beberapa spesies lainnya.",
        "Pilihan wisata selain menyelam adalah melihat proses bertelur penyu dan juga menikmati pantai yang bersih nan indah. Fasilitas komunikasi di Kepulauan Derawan sudah baik, sebagai contohnya adalah telah tersedianya sinyal 3G.Resort dan Penginapan yang ada di Kepulauan Derawan banyak tersebar di pinggir pantai, dengan harga yang lebih murah dibandingkan dengan tempat wisata di Bali maupun di Lombok.",
        "Kegiatan perikanan merupakan tulang punggung perekonomian masyarakat Kepulauan Derawan sebab sebagian besar penduduknya bermata pencaharian sebagai nelayan. Perikanan yang dimanfaatkan oleh masyarakat Kepulauan Derawan adalah ikan pelagis dan ikan karang. ",
        //RAJA AMPAT 40
        "Kepulauan Raja Ampat merupakan rangkaian empat gugusan pulau yang berdekatan dan berlokasi di barat bagian Kepala Burung (Vogelkoop) Pulau Papua. Secara administrasi, gugusan ini berada di bawah Kabupaten Raja Ampat, Provinsi Papua Barat. ", 
        "Masyarakat Kepulauan Raja Ampat umumnya nelayan tradisional yang berdiam di kampung-kampung kecil yang letaknya berjauhan dan berbeda pulau. Mereka adalah masyarakat yang ramah menerima tamu dari luar. ",
        "Kepulauan Raja Ampat merupakan tempat yang sangat berpotensi untuk dijadikan sebagai objek wisata, terutama wisata penyelaman. Perairan Kepulauan Raja Ampat menurut berbagai sumber, merupakan salah satu dari 10 perairan terbaik untuk diving site di seluruh dunia. Bahkan, mungkin juga diakui sebagai nomor satu untuk kelengkapan flora dan fauna bawah air pada saat ini.", 
        "Karena daerahnya yang banyak pulau dan selat sempit, maka sebagian besar tempat penyelaman pada waktu tertentu memiliki arus yang kencang. Hal ini memungkinkan juga untuk melakukan drift dive, menyelam sambil mengikuti arus yang kencang dengan air yang sangat jernih sambil menerobos kumpulan ikan.",
        "Empat gugusan pulau yang menjadi anggotanya dinamakan menurut empat pulau terbesarnya, yaitu Pulau Waigeo, Pulau Misool, Pulau Salawati, dan Pulau Batanta.",
        //BUNAKEN 45
        "Bunaken adalah nama sebuah pulau seluas 8,08 km² di Teluk Manado, yang terletak di utara pulau Sulawesi, Indonesia. Pulau ini merupakan bagian dari kota Manado, ibu kota provinsi Sulawesi Utara, Indonesia.  ",
        "Di sekitar pulau Bunaken terdapat Taman Laut Bunaken yang juga sering disebut sebagai Taman Nasional Bunaken. Taman laut ini memiliki biodiversitas kelautan salah satu yang tertinggi di dunia.",
        "Olah raga yang menjadi andalan di Taman Laut Bunaken adalah selam scuba. Secara keseluruhan Taman Laut Bunaken ini memiliki luas area 75.265 hektar dengan lima pulau didalamnya antara lain : ",
        "Pulau Bunaken itu sendiri, Pulau Manado Tua, Pulau Siladen, Pulau Naen, dan Pulau Mantehage beserta  beberapa anak Pulau Mantehage. Meskipun meliputi berbagai pulau namun kegiatan diving sangat dibatasi. ",
        "Selain diperuntukan diving, wisatawan juga dapat melakukan dive spot karena di Taman Laut ini memiliki 20 titik penyelaman dengan kedalaman yang bervariasi hingga 1.334 meter.",

    };

	void Awake ()
	{
        //		myRect = ProgressBar.GetComponent<RectTransform> ();
        //		PB_xSize = (float)ProgressBar.GetComponent<RectTransform> ().rect.width/4;
        //		myRect.sizeDelta = new Vector2 (0,myRect.rect.height);
        LevelToLoad = PlayerPrefs.GetString("LevelToLoad", "C#");

	}


	void Start () {
        Time.timeScale = 1;
        #region NextContentTest
        /*
        if (LevelLoad != 8)
        {
            StartCoroutine(WaitingScene());
            TextContent.text = ContentLoading[0 + LevelLoadedText];
        }

        if (LevelLoad == 8)
        {
            StartCoroutine(WaitingNext());
            Button_Next.SetActive(false);
            TextContent.text = ContentLoading[count + LevelLoadedText];
        }*/

        //StartCoroutine(WaitingNext());
        //Button_Next.SetActive(false);
        StartCoroutine(AsynchronousLoad(LevelToLoad));
        LevelLoadedText = PlayerPrefs.GetInt("TextContentLevel", 0);
        TextContent.text = ContentLoading[count + LevelLoadedText];



#endregion

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            //Application.LoadLevel(LevelLoad);
            //fillBarr.fillAmount = 0.2f;
        }
    }
	
	

    IEnumerator AsynchronousLoad(string scene)
    {
        yield return null;

        AsyncOperation ao = SceneManager.LoadSceneAsync(scene);
        ao.allowSceneActivation = false;

        while (!ao.isDone)
        {
            // [0, 0.9] > [0, 1]
            float progress = Mathf.Clamp01(ao.progress / 0.9f);
            fillBarr.fillAmount = Mathf.Lerp(fillBarr.fillAmount,progress,Time.deltaTime);
            Debug.Log("Loading progress: " + (progress * 100) + "%");

            if(fillBarr.fillAmount>=0.99){
                yield return new WaitForSeconds(3f);
                Debug.Log("Press a key to start");
                ButtontoLoadLevel.SetActive(true);
            }
            // Loading completed
            //if (ao.progress == 0.9f)

            yield return null;
        }
    }

    public void ButtonLoadLevel(){
        SceneManager.LoadScene(LevelToLoad);
    }

    IEnumerator WaitingScene()
    {
		while (true) {
			yield return new WaitForSeconds (4f);
			count++;
            TextContent.text = ContentLoading[count+LevelLoadedText];
			if (count<=4)
				//myRect.sizeDelta = new Vector2 (myRect.rect.width+PB_xSize,myRect.rect.height);
			if (count == 4) {
//                StartCoroutine(LevelCoroutine());

			}
		}
       
    }


    IEnumerator WaitingNext()
    {
        yield return new WaitForSeconds(2f);

    }

    public void CanNextText()
    {
        //if (CanNext)
        //{
            count++;
            if (count <= 4)
            {
                TextContent.text = ContentLoading[count + LevelLoadedText];
                //myRect.sizeDelta = new Vector2(myRect.rect.width + PB_xSize, myRect.rect.height);
            }
            //Button_Next.SetActive(false);
            //Button_Back.SetActive(false);

            //StartCoroutine(WaitingNext());
        //}
        //if (count > 4)
        //{
        //    StartCoroutine(LevelCoroutine());
        //}
            
    }

    public void CanBackText()
    {
        if (count>0)
        {
            count--;
            if (count <= 4)
            {
                TextContent.text = ContentLoading[count + LevelLoadedText];
                //myRect.sizeDelta = new Vector2(myRect.rect.width - PB_xSize, myRect.rect.height);
            }
            //Button_Next.SetActive(false);
            //Button_Back.SetActive(false);

            //StartCoroutine(WaitingNext());
        }
        

    }

    public void NextTextExplain()
    {
        if (count >= 0 && count < 4)
        {
            count++;
            TextContent.text = ContentLoading[count + LevelLoadedText];
            setOnOffButton();
        }
    }
    public void BackTextExplain(){
        if (count > 0 && count <= 4)
        {
            count--;
            TextContent.text = ContentLoading[count + LevelLoadedText];
            setOnOffButton();
        }
    }

    void setOnOffButton(){
        if (count == 0)
        {
            Button_Back.SetActive(false);
        }
        else
        {
            Button_Back.SetActive(true);
        }

        if (count == 4)
        {
            Button_Next.SetActive(false);
        }
        else
        {
            Button_Next.SetActive(true);
        }
    }
}
