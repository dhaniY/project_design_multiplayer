﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AchievementHandler : MonoBehaviour {

	public Text _TotPoint;
	public Text _Highscore;

    public GameObject[] Bintang;

    public int HighScore,CurrentScore;
    public int NowLevelWhat=0;
    // TEMBAK = 1 , FISHING = 2 , DIVING = 3, BALAP = 4, DEFEND = 5. 
    public Sprite[] IconSprite;

    public Image SprRndr;

    //public int Level = 1;
	// Use this for initialization
	void Start () {
        NowLevelWhat =  PlayerPrefs.GetInt("NowLevelWhat", 0);

        print("point1: " + PlayerPrefs.GetInt("LastPoint_Balap_1") + "highpoint1: " + PlayerPrefs.GetInt("Highscore_Balap_1"));
        print("point2: " + PlayerPrefs.GetInt("LastPoint_Balap_2") + "highpoint1: " + PlayerPrefs.GetInt("Highscore_Balap_2"));
        //SprRndr = GameObject.Find("Canvas").transform.FindChild("IconGame").GetComponent<Image>();
        Time.timeScale = 1;

        if (NowLevelWhat == 1)
        {
            if (PlayerPrefs.GetInt("Level_tembak") == 1)
            {
                SprRndr.sprite = IconSprite[0];
                _TotPoint.text = PlayerPrefs.GetInt("point_tembak_1").ToString();
                CurrentScore = PlayerPrefs.GetInt("point_tembak_1");
                if (CurrentScore >= PlayerPrefs.GetInt("HighScore1"))
                {
                    HighScore = CurrentScore;
                    PlayerPrefs.SetInt("HighScore1", HighScore);
                }

                //_Star.text = "" + PlayerPrefs.GetInt("HighScore1");
                PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold", 0) + (CurrentScore / 2));
                PlayerPrefs.DeleteKey("Level_tembak");
            }

            else if (PlayerPrefs.GetInt("Level_tembak") == 2)
            {
                SprRndr.sprite = IconSprite[0];
                _TotPoint.text = PlayerPrefs.GetInt("point_tembak_2").ToString();
                CurrentScore = PlayerPrefs.GetInt("point_tembak_2");
                if (CurrentScore >= PlayerPrefs.GetInt("HighScore2"))
                {
                    HighScore = CurrentScore;
                    PlayerPrefs.SetInt("HighScore2", HighScore);
                }

                //_Star.text = "" + PlayerPrefs.GetInt("HighScore2");
                PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold", 0) + (CurrentScore / 2));
                PlayerPrefs.DeleteKey("Level_tembak");
            }
        }

        ///////////////Fishing////////////////////////////////////////////////////////////////////
        if (NowLevelWhat == 2)
        {
            Debug.Log("NowLevelWhat Called");
            if (PlayerPrefs.GetInt("levelFishing") == 1)
            {
                Debug.Log("LevelFishingCalled");
                SprRndr.sprite = IconSprite[1];
                _TotPoint.text = "" + PlayerPrefs.GetInt("point_fishing_1");
                CurrentScore = PlayerPrefs.GetInt("point_fishing_1");
                if (CurrentScore >= PlayerPrefs.GetInt("HighScore1_Fishing"))
                {
                    HighScore = CurrentScore;
                    PlayerPrefs.SetInt("HighScore1_Fishing", HighScore);
                }

                _Highscore.text = "" + PlayerPrefs.GetInt("HighScore1_Fishing");
                PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold", 0) + (CurrentScore / 2));
                PlayerPrefs.DeleteKey("Level_fishing");
            }

            else if (PlayerPrefs.GetInt("levelFishing") == 2)
            {
                SprRndr.sprite = IconSprite[1];
                _TotPoint.text = "" + PlayerPrefs.GetInt("point_fishing_2");
                CurrentScore = PlayerPrefs.GetInt("point_fishing_2");
                if (CurrentScore >= PlayerPrefs.GetInt("HighScore2_Fishing"))
                {
                    HighScore = CurrentScore;
                    PlayerPrefs.SetInt("HighScore2_Fishing", HighScore);
                }

                _Highscore.text = "" + PlayerPrefs.GetInt("HighScore2_Fishing");
                PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold", 0) + (CurrentScore / 2));
                PlayerPrefs.DeleteKey("Level_fishing");
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////

        if (NowLevelWhat == 3)
        {
            if (PlayerPrefs.GetInt("LevelGameDiving") == 1)
            {
                SprRndr.sprite = IconSprite[3];
                CurrentScore = PlayerPrefs.GetInt("LastPoint_Diving_1");
                _TotPoint.text = "" + CurrentScore;
                HighScore = PlayerPrefs.GetInt("Highscore_Diving_1", 0);
                if (CurrentScore >= HighScore)
                {
                    HighScore = CurrentScore;
                    PlayerPrefs.SetInt("Highscore_Diving_1", HighScore);
                }

                _Highscore.text = "" + PlayerPrefs.GetInt("Highscore_Diving_1");
                //PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold", 0) + (CurrentScore / 2));
                PlayerPrefs.DeleteKey("LevelGameDiving");
            }

            else if (PlayerPrefs.GetInt("LevelGameDiving") == 2)
            {
                SprRndr.sprite = IconSprite[3];
                CurrentScore = PlayerPrefs.GetInt("LastPoint_Diving_2");
                _TotPoint.text = "" + CurrentScore;
                HighScore = PlayerPrefs.GetInt("Highscore_Diving_2", 0);
                if (CurrentScore >= HighScore)
                {
                    HighScore = CurrentScore;
                    PlayerPrefs.SetInt("Highscore_Diving_2", HighScore);
                }

                _Highscore.text = "" + PlayerPrefs.GetInt("Highscore_Diving_2");
                //PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold", 0) + (CurrentScore / 2));
                PlayerPrefs.DeleteKey("LevelGameDiving");
            }

            
        }

        /////////////////////////////////////////////////////////////////////////////////////////

        if (NowLevelWhat == 4)
        {
            if (PlayerPrefs.GetInt("LevelGameBalap") == 1)
            {
                SprRndr.sprite = IconSprite[2];
                CurrentScore = PlayerPrefs.GetInt("LastPoint_Balap_1");
                _TotPoint.text = "" + CurrentScore;
                HighScore = PlayerPrefs.GetInt("Highscore_Balap_1", 0);
                if (CurrentScore >= HighScore)
                {
                    HighScore = CurrentScore;
                    PlayerPrefs.SetInt("Highscore_Balap_1", HighScore);
                }

                _Highscore.text = "" + PlayerPrefs.GetInt("Highscore_Balap_1");
                //PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold", 0) + (CurrentScore / 2));
                PlayerPrefs.DeleteKey("LevelGameBalap");
            }

            else if (PlayerPrefs.GetInt("LevelGameBalap") == 2)
            {
                SprRndr.sprite = IconSprite[2];
                CurrentScore = PlayerPrefs.GetInt("LastPoint_Balap_2");
                _TotPoint.text = "" + CurrentScore;
                HighScore = PlayerPrefs.GetInt("Highscore_Balap_2", 0);
                if (CurrentScore >= HighScore)
                {
                    HighScore = CurrentScore;
                    PlayerPrefs.SetInt("Highscore_Balap_2", HighScore);
                }

                _Highscore.text = "" + PlayerPrefs.GetInt("Highscore_Balap_2");
                //PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold", 0) + (CurrentScore / 2));
                PlayerPrefs.DeleteKey("LevelGameBalap");
            }
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////

        if (NowLevelWhat == 5)
        {
            if (PlayerPrefs.GetInt("LevelGameDefense") == 1)
            {
                SprRndr.sprite = IconSprite[4];
                CurrentScore = PlayerPrefs.GetInt("LastPoint_Defense_1");
                _TotPoint.text = "" + CurrentScore;
                HighScore = PlayerPrefs.GetInt("Highscore_Defense_1", 0);
                if (CurrentScore >= HighScore)
                {
                    HighScore = CurrentScore;
                    PlayerPrefs.SetInt("Highscore_Defense_1", HighScore);
                }

                _Highscore.text = "" + PlayerPrefs.GetInt("Highscore_Defense_1");
                //PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold", 0) + (CurrentScore / 2));
                PlayerPrefs.DeleteKey("LevelGameDefense");
            }

            else if (PlayerPrefs.GetInt("LevelGameDefense") == 2)
            {
                SprRndr.sprite = IconSprite[4];
                CurrentScore = PlayerPrefs.GetInt("LastPoint_Defense_2");
                _TotPoint.text = "" + CurrentScore;
                HighScore = PlayerPrefs.GetInt("Highscore_Defense_2", 0);
                if (CurrentScore >= HighScore)
                {
                    HighScore = CurrentScore;
                    PlayerPrefs.SetInt("Highscore_Defense_2", HighScore);
                }

                _Highscore.text = "" + PlayerPrefs.GetInt("Highscore_Defense_2");
                //PlayerPrefs.SetInt("Gold", PlayerPrefs.GetInt("Gold", 0) + (CurrentScore / 2));
                PlayerPrefs.DeleteKey("LevelGameDefense");
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////

        if (PlayerPrefs.GetInt("pointBintang") == 1)
        {
            Bintang[0].SetActive(true);
        }

        if (PlayerPrefs.GetInt("pointBintang") == 2)
        {
            Bintang[0].SetActive(true);
            Bintang[1].SetActive(true);
        }

        if (PlayerPrefs.GetInt("pointBintang") == 3)
        {
            Bintang[0].SetActive(true);
            Bintang[1].SetActive(true);
            Bintang[2].SetActive(true);
        }
	}
	
	// Update is called once per frame
	//void Update () {
	
	//}
}
