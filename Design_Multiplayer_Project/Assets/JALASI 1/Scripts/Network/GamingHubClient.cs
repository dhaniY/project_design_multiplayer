﻿using Grpc.Core;
using MagicOnion.Client;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using static GamingHub;

public class GamingHubClient : MonoBehaviour, IGamingHubReceiver
{
    private static GamingHubClient _instance;
    public static GamingHubClient Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        DontDestroyOnLoad(this);
    }
    public string _Channel;
    public int _port;

    public Dictionary<string, Player> players = new Dictionary<string, Player>();
    public Dictionary<string, GameObject> playersOBJ = new Dictionary<string, GameObject>();
    public GameObject PerahuPlayer;
    public GameObject PerahuOtherPlayer;

    public string PlayerName;
    public IGamingHub client;

    bool isInGame { set;  get; }
    public GameObject playerLobby;
    public GameObject objLobby;

    private GPGSHandler _GPGSHandler;

    private void Start()
    {
        _GPGSHandler = this.gameObject.GetComponent<GPGSHandler>();
        isInGame = false;
        PlayerName = "Player" + Random.Range(0, 100);
    }

    private void OnDestroy()
    {
        LeaveAsync();
    }
    private void Update()
    {
        Scene _scene = SceneManager.GetActiveScene();
        if (_scene.name == "Balap1 Bawean")
        {
            if (players[PlayerName].isReady == false)
            {
                ChangeStatus(true);
            }
        }
    }
    public void JoinLobby()
    {
        var channel = new Channel(_Channel, _port, ChannelCredentials.Insecure);
        ConnectAsync(channel, "BoatRush", PlayerName);
    }

    public void LeaveLobby()
    {
        LeaveAsync();
    }

    public async Task<Player> ConnectAsync(Channel grpcChannel, string roomName, string playerName)
    {
        client = StreamingHubClient.Connect<IGamingHub, IGamingHubReceiver>(grpcChannel, this);

        var roomPlayers = await client.JoinAsync(roomName, playerName, Vector3.zero, Quaternion.identity);
        foreach (var player in roomPlayers)
        {
            if (!players.ContainsKey(player.Name))
            {
                players.Add(player.Name, player);
            }
        }

        return players[playerName];
    }

    // methods send to server.

    public async Task LeaveAsync()
    {
        await client.LeaveAsync();
        return;
    }

    public async Task MoveAsync(Vector3 position, Quaternion rotation)
    {
        await client.MoveAsync(position, rotation);
        return;
    }

    public async Task ChangeStatus(bool _isReady)
    {
        await client.ChangeStatus(_isReady);
        return;
    }

    public async Task ChangeScene(string _nameScene)
    {
        await client.ChangeScene(_nameScene);
        return;
    }

    // dispose client-connection before channel.ShutDownAsync is important!
    public async Task DisposeAsync()
    {
        await client.DisposeAsync();
        return;
    }

    // You can watch connection state, use this for retry etc.
    public async Task WaitForDisconnect()
    {
        await client.WaitForDisconnect();
        return;
    }

    // Receivers of message from server.

    void IGamingHubReceiver.OnJoin(Player player)
    {
        if (isInGame)
        {
            return;
        }
        else
        {
            Debug.Log("Join Player:" + player.Name);

            //var boat = GameObject.CreatePrimitive(PrimitiveType.Cube);
            //boat.name = player.Name;
            //boat.transform.SetPositionAndRotation(player.Position, player.Rotation);
            Player newPlayer = new Player();
            newPlayer.Name = player.Name;
            newPlayer.Position = player.Position;
            newPlayer.Rotation = player.Rotation;
            players[player.Name] = newPlayer;
            var newPlayerLobby = Instantiate(playerLobby, objLobby.transform);
            newPlayerLobby.GetComponent<ObjPlayerLobby>().playerName.text = newPlayer.Name;
            Debug.Log("Number of Player " + players.Count);
        }

    }

    void IGamingHubReceiver.OnLeave(Player player)
    {
        Debug.Log("Leave Player:" + player.Name);

        if (players.TryGetValue(player.Name, out var cube))
        {
            //GameObject.Destroy(cube);
        }
    }

    void IGamingHubReceiver.OnMove(Player player)
    {
        Debug.Log("Move Player:" + player.Name);

        if (players.TryGetValue(player.Name, out var cube))
        {
            //cube.transform.SetPositionAndRotation(player.Position, player.Rotation);
        }
        playersOBJ[player.Name].transform.SetPositionAndRotation(player.Position, player.Rotation);
    }

    void IGamingHubReceiver.OnChangeStatus(Player player)
    {
        Debug.Log("Move Player:" + player.Name + "/" + player.isReady);
        if (players.TryGetValue(player.Name, out var _player))
        {
            _player.isReady = player.isReady;
        }
    }

    void IGamingHubReceiver.OnChangeScene(string nameScene)
    {
        SceneManager.LoadScene(nameScene);
    }

    public void InstantiateAllPlayer()
    {
        foreach (var player in players)
        {
            if (player.Key == PlayerName)
            {
                var _player = Instantiate(PerahuPlayer, new Vector3(0,0,0), Quaternion.identity);
                playersOBJ.Add(PlayerName, _player);
            }
            else
            {
                var _OtherPlayer = Instantiate(PerahuOtherPlayer, new Vector3(Random.Range(-6.1f, 6.1f),
                    Random.Range(-2.1f, 3.1f),0), Quaternion.identity);
                playersOBJ.Add(player.Key, _OtherPlayer);
            }
        }
    }
}
