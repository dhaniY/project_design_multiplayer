﻿using Grpc.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectionHandler : MonoBehaviour
{
    private Channel channel;

    GamingHubClient _client;

    public GameObject playerLobby;
    // Start is called before the first frame update
    void Start()
    {
        _client = new GamingHubClient();
        InitializeClient();
    }

    public void InitializeClient()
    {
        Debug.Log("Called");
        var channel = new Channel("localhost:100", ChannelCredentials.Insecure);
        _client.ConnectAsync(channel, "BoatRush", "Player1");
    }
    async void OnDestroy()
    {
        await this.channel.ShutdownAsync();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
