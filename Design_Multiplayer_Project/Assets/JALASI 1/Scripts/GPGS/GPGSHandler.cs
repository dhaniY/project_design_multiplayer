﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

public class GPGSHandler : MonoBehaviour
{
    public static PlayGamesPlatform platform;
    public string UserName;
    public string UserID;
    public Texture2D ProfilePic;

    private void Start()
    {
        if (platform == null)
        {
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
            PlayGamesPlatform.InitializeInstance(config);
            // recommended for debugging:
            PlayGamesPlatform.DebugLogEnabled = true;
            // Activate the Google Play Games platform
            PlayGamesPlatform.Activate();
        }
        Social.Active.localUser.Authenticate(success =>
        {
            if (success)
            {
                UserName = Social.localUser.userName; // UserName
                UserID = Social.localUser.id; // UserID
                ProfilePic = Social.localUser.image; // ProfilePic
                GamingHubClient.Instance.PlayerName = UserName;
            }
            else
            {
                Debug.LogWarning("Failed to authenticate");
            }

        });
    }
 

}
