﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenuManager : MonoBehaviour {
    bool isSettingOn = false, isMenuOn = false, isBGMOn = true, isSFXon = true;
    public GameObject Setting_On, Menu_On, BGM_Button_On, BGM_Button_Off, SFX_Button_On, SFX_Button_Off;

    void Start()
    {
        Time.timeScale = 1;
        if (PlayerPrefs.GetInt("BGM_INGAME") == 0) { BGM_Button_On.SetActive(false); BGM_Button_Off.SetActive(true); }
        if (PlayerPrefs.GetInt("SFX_INGAME") == 0) { SFX_Button_On.SetActive(false); SFX_Button_Off.SetActive(true); }
    }
    public void SettingUp()
    {
        if (!isSettingOn)
        {
            Setting_On.SetActive(true);
        }

        if (isSettingOn)
        {
            Setting_On.SetActive(false);
        }

        isSettingOn = !isSettingOn;
    }

    public void MenuUp()
    {
        if (!isMenuOn)
        {
            Menu_On.SetActive(true);
        }

        if (isMenuOn)
        {
            Menu_On.SetActive(false);
        }

        isMenuOn = !isMenuOn;
    }

    public void SoundBGMHandler()
    {
        if (isBGMOn)
        {
            PlayerPrefs.SetInt("BGM_INGAME", 0);
            BGM_Button_Off.SetActive(true);
            BGM_Button_On.SetActive(false);
            print("Mati");
        }

        if (!isBGMOn)
        {
            PlayerPrefs.SetInt("BGM_INGAME", 1);
            BGM_Button_On.SetActive(true);
            BGM_Button_Off.SetActive(false);
            print("Hidup");
        }

        isBGMOn = !isBGMOn;

    }

    public void SoundSFXHandler()
    {
        if (isSFXon)
        {
            PlayerPrefs.SetInt("SFX_INGAME", 0);
            SFX_Button_Off.SetActive(true);
            SFX_Button_On.SetActive(false);
        }

        if (!isSFXon)
        {
            PlayerPrefs.SetInt("SFX_INGAME", 1);
            SFX_Button_On.SetActive(true);
            SFX_Button_Off.SetActive(false);
        }

        isSFXon = !isSFXon;

    }

    public void GoToFacebook(string url_link)
    {
        Application.OpenURL(url_link);
    }

}
