﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Navigation : MonoBehaviour {

    public GameObject LoadingPanel;
    public string BackLevel;
	// Use this for initialization
	void Start () {

        //print(PlayerPrefs.GetInt("level2") + "Levelnya");
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Application.loadedLevelName == "MainMenu")
            {
                Application.Quit();
                print("Quit");
            }else        
                Application.LoadLevel(BackLevel);
        }
	
	}

	public void NextGame()
	{
		string sceneGame = PlayerPrefs.GetString ("AfterShop");
		if (sceneGame == "GameScene") {
			if (PlayerPrefs.GetInt ("curLevel") == 2)
				Application.LoadLevel ("GameScene2");
			else
				Application.LoadLevel (sceneGame);
		} 
		else {
			print ("test2");
			Application.LoadLevel (sceneGame);
		}

	}

    public void QuitGame()
    {
        Application.Quit();
        print("Quit");
    }



    public void GotoLevel(string scene)
    {
        Application.LoadLevel(scene);
        //LoadingPanel.SetActive(true);
        //GameObject.Find("PanelLoading").SetActive(true);
    }

    public void GotoLevel2(string scene)
    {
        if (PlayerPrefs.GetInt("IsAfterFishing") == 0)
        {
            //Application.LoadLevel(scene);
            SceneManager.LoadScene(scene);
        }
        else
        {
            Application.LoadLevel("MainMenu");
        }
    }

    public void SetterLevelTembak2(int Level)
    {
        PlayerPrefs.SetInt("level2", Level);
        print("Dan Levelnya " + Level);
    }

    public void SetterLevelTembak1(int Level)
    {
        PlayerPrefs.SetInt("level1", Level);
        print("Dan Levelnya " + Level);
    }

	public void SetterLevelFishing(int Level)
	{
		PlayerPrefs.SetInt("levelFishing", Level);
		print("Dan Levelnya " + Level);
	}

	public void SetterLevelDiving(int Level)
	{
		PlayerPrefs.SetInt("levelDiving", Level);
		print("Dan Levelnya " + Level);
	}


    public void AfterGOver()
    {
        if (PlayerPrefs.GetInt("AfterGOver") == 1)
        {
            Application.LoadLevel("GameScene");
        }
        else if (PlayerPrefs.GetInt("AfterGOver") == 2)
        {
            Application.LoadLevel("GameScene2");
        }
    }

	public void RetryGameFishing()
	{
		string scene = PlayerPrefs.GetString ("mylevelfish");
		Application.LoadLevel (scene);

	}

    public void SelectLevel(int Level)
    {
        PlayerPrefs.SetInt("LevelSelection", Level);
    }

}
