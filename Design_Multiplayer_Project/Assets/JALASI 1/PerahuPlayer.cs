﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class PerahuPlayer : MonoBehaviour {
    public int LevelGameBalap = 0;

    public float ForceKapal = 1;
	public float HP = 100;

	public Image Healthbar;

	public Text HpText;
    Rigidbody2D PlayerRig;
    
	// Use this for initialization
	void Start () {
        PlayerRig = gameObject.GetComponent<Rigidbody2D>();
        Healthbar = GameObject.Find("BarHp").GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 MoveJoystick = new Vector2(CrossPlatformInputManager.GetAxis("Horizontal"), CrossPlatformInputManager.GetAxis("Vertical")*ForceKapal);
		gameObject.transform.Translate(MoveJoystick * Time.deltaTime);
        BroadCastMovement();
        if (HP >=0){
		//HpText.text = HP + "/100";
		Healthbar.GetComponent<Image>().fillAmount = HP/100;
			if(HP <= 0){
				Destroy(GameObject.Find("Player"));
                GamingHubClient.Instance.LeaveAsync();
				GameObject.FindObjectOfType<Balap_GameManager>().GameStart = false;
                Evaluate();
			}
		}

			gameObject.transform.rotation = Quaternion.identity;

		//PlayerRig.velocity = Vector3.ClampMagnitude(PlayerRig.velocity, 0.7f);
        //PlayerRig.AddForce(MoveJoystick);
		//barHp.GetComponent<Image>().fillAmount = ;
      
	}

    async void BroadCastMovement()
    {
        GamingHubClient.Instance.MoveAsync(gameObject.transform.position, gameObject.transform.rotation);
    }

	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag == "Enemy")
		{
			GameObject.FindObjectOfType<Shake>().shakes();
			StartCoroutine(getDamage());
			HP -= 10;
			//Destroy(other.gameObject);
		}
	}


	IEnumerator getDamage(){
		gameObject.GetComponent<SpriteRenderer>().color = Color.red;
		yield return new WaitForSeconds(0.05f);
		gameObject.GetComponent<SpriteRenderer>().color = Color.white;
		yield return new WaitForSeconds(0.06f);
		gameObject.GetComponent<SpriteRenderer>().color = Color.red;
		yield return new WaitForSeconds(0.06f);
		gameObject.GetComponent<SpriteRenderer>().color = Color.white;
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Collectible")
        {
            GameObject.Find("GameManager").GetComponent<Balap_GameManager>().AddPoint();
            Destroy(other.gameObject);
        }

		if (other.gameObject.tag == "quiz")
		{
			GameObject.FindObjectOfType<Quiz>().showSoalDefender();
            Destroy(other.gameObject);
		}

    }

    public void Evaluate()
    {
        int savePoint = GameObject.Find("GameManager").GetComponent<Balap_GameManager>().PoinPlayer;
        if (savePoint <= 30)
            PlayerPrefs.SetInt("pointBintang", 1);
        else if (savePoint <= 60)
            PlayerPrefs.SetInt("pointBintang", 2);
        else
            PlayerPrefs.SetInt("pointBintang", 3);


        PlayerPrefs.SetInt("LevelGameBalap", LevelGameBalap);
        if (LevelGameBalap == 1)
        {
            PlayerPrefs.SetInt("LastPoint_Balap_1", savePoint);
        }
        if (LevelGameBalap == 2)
        {
            PlayerPrefs.SetInt("LastPoint_Balap_2", savePoint);
        }

        Application.LoadLevel("Achievement");
        
    }
}
