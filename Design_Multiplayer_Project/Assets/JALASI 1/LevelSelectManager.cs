﻿using UnityEngine.UI;
using System.Collections;
using UnityEngine;

public class LevelSelectManager : MonoBehaviour {
    int Levelnya = 0;
    public GameObject TembakGame,DefenseGame,DivingGame,FishingGame,BalapGame;

    public Button[] LevelQuiz;
    public Button[] LevelFishing;
    public Button[] LevelDiving;
    public Button[] LevelRace;
    public Button[] LevelAttack;
	// Use this for initialization
	void Start ()
    {
        int levelSelection = PlayerPrefs.GetInt("LevelSelection", 0);
        if(levelSelection == 0){
            TembakGame.gameObject.SetActive(true);
        }else if(levelSelection == 1){
            FishingGame.gameObject.SetActive(true);
        }else if (levelSelection == 2)
        {
            DivingGame.gameObject.SetActive(true);
        }else if (levelSelection == 3)
        {
            BalapGame.gameObject.SetActive(true);
        }else if (levelSelection == 4)
        {
            DefenseGame.gameObject.SetActive(true);
        }


        int openLevel_Quiz = PlayerPrefs.GetInt("OpenLevel_Quiz", 0);
        for (int i = 4; i > openLevel_Quiz; i--)
        {
            LevelQuiz[i].interactable = false;
        }

        int openLevel_Fishing = PlayerPrefs.GetInt("OpenLevel_Fishing", 0);
        for (int i = 4; i > openLevel_Quiz; i--)
        {
            LevelFishing[i].interactable = false;
        }

        int openLevel_Diving = PlayerPrefs.GetInt("OpenLevel_Diving", 0);
        for (int i = 4; i > openLevel_Quiz; i--)
        {
            LevelDiving[i].interactable = false;
        }

        int openLevel_Race = PlayerPrefs.GetInt("OpenLevel_Race", 0);
        for (int i = 4; i > openLevel_Quiz; i--)
        {
            LevelRace[i].interactable = false;
        }

        int openLevel_Attack = PlayerPrefs.GetInt("OpenLevel_Attack", 0);
        for (int i = 4; i > openLevel_Quiz; i--)
        {
            LevelAttack[i].interactable = false;
        }



    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void LoadtoLevel(string levelGo){
        PlayerPrefs.SetString("LevelToLoad", levelGo);
    }
    public void LoadtoTextLevel(int textlevelGo)
    {
        PlayerPrefs.SetInt("TextContentLevel", textlevelGo);

    }

}
